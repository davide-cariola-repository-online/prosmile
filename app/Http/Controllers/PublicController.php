<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;


class PublicController extends Controller
{
    public function homePage(){
        return view('homepage');
    }

    public function contatti(){
        return view('contatti');

    }


    public function chisiamo(){
        $teams = [
            ["id" => "1", "name" => "Dott. Francesco Talamona", "role" => "Direttore Sanitario", "cv" => "Laureato in Medicina e Chirurgia presso l'Università di Studi di Roma La Sapienza. Iscritto all'Albo degli Odontoiatri di Roma n.627x. Master in Implantoprotesi. Master in Chirurgia Orale. Collabora con proSMILE dal 2014.", "photo" => "/Media/francesco-talamona.png"],
            ["id" => "2", "name" => "Dott. Simone Fiore", "role" => "Odontoiatra Senior", "cv" => "Laureato in Odontoiatria e Protesi Dentaria presso l'Università degli Studi di Milano. Iscritto all'Albo degli Odontoiatri di Brescia n.998x. 70 pubblicazioni su odontoiatria, protesi e gnatologia. Professore Emerito presso l'Università di Milano e di Brescia. Collabora con proSMILE dal 2014.", "photo" => "/Media/simone-fiore.png"],
            ["id" => "3", "name" => "Dott. Andrea Gernone", "role" => "Odontoiatra Senior", "cv" => "Laureato in Medicina e Chirurgia presso l'Università degli Studi di Bari. Iscritto all'Albo degli Odontoiatri di Bari n.352x. 20 pubblicazioni su riviste scientifiche nazionali ed internazionali. Master in Medicina Estetica. Collabora con proSMILE dal 2016.", "photo" => "/Media/andrea-gernone.png"],
            ["id" => "4", "name" => "Dott. Marco Insabato", "role" => "Odontoiatra Junior", "cv" => "Laureato in Odontoiatria e Protesi Dentaria presso l'Università degli Studi di Lecce. Iscritto all'Albo degli Odontoiatri di Bari n.921x. Master in Endodonzia. Master in Odontoiatria restaurativa. Collabora con proSMILE dal 2018.", "photo" => "/Media/marco-insabato.png"],
            ["id" => "5", "name" => "Dott. Antonio Masoni", "role" => "Odontoiatra Junior", "cv" => "Laureato in Medicina e Chirurgia presso l'Università degli Studi di Pavia. Iscritto all'Albo degli Odontoiatri di Pavia n.751x. Master in Odontostomatologia. Master in Odontoiatria restaurativa. Collabora con proSMILE dal 2018.", "photo" => "/Media/antonio-masoni.png"],
            ["id" => "6", "name" => "Dott. Matteo Sisto", "role" => "Odontoiatra Junior", "cv" => "Laureato in Odontoiatria e Protesi Dentaria presso l'Università degli Studi di Bari. Iscritto all'Albo degli Odontoiatri di Bari n.673x. Master in Endodonzia. Master in Odontoiatria restaurativa. Collabora con proSMILE dal 2018.", "photo" => "/Media/matteo-sisto.png"],
            ["id" => "7", "name" => "Davide Cariola", "role" => "Spotlight Web Developing", "cv" => "Sviluppatore Full-stack con un passato nel fashion retail. Abile con HTML5, CSS3, JavaScript, PHP8 e Laravel8. Collabora con proSMILE dal 2021.", "photo" => "/Media/davide-cariola.png"]
        ];

        return view('chisiamo', ['team' => $teams]);
    }

    public function servizi(){
        $services = [
            ["id" => "1", "name" => "Igiene dentale", "description" => "La pulizia dei denti, ovvero il trattamento di Igiene Dentale Professionale è spesso il primo al quale si è sottoposti dopo la Visita Specialistica.", "img" => "/Media/igiene-dent.png"],
            ["id" => "2", "name" => "Implantologia", "description" => "L’Implantologia Dentale è quella branca dell’Odontoiatria che si occupa di sostituire i denti mancanti con altrettante radici sintetiche ancorate nell’osso.", "img" => "/Media/impla.png"],
            ["id" => "3", "name" => "Paradontologia", "description" => "La Parodontologia è la branca dell’odontoiatria che studia i tessuti del parodonto e le patologie ad esso correlate, definite appunto malattie parodontali.", "img" => "/Media/Paradontologiamin.png"],
            ["id" => "4", "name" => "Faccette dentali", "description" => "Le Faccette Dentali Estetiche vengono realizzate con una ceramica di ultimissima generazione, sue proprietà sono del tutto simili a quelle dello smalto dentale.", "img" => "/Media/faccdent-min.png"],
            ["id" => "5", "name" => "Sbiancamento Professionale", "description" => "Per effetuare lo sbiancamento dei denti l’operatore applicherà una sostanza isolante sulle vostre gengive che garantirà la protezione dei vostri tessuti molli.", "img" => "/Media/sbdenti-min.png"],
            ["id" => "6", "name" => "Ortodonzia Invisibile", "description" => "L’Ortodonzia è quella branca dell’odontoiatria che si occupa delle diverse anomalie della costituzione, dello sviluppo e della posizione dei denti e delle ossa mascellari.", "img" => "/Media/ortodonz-min.png"],
            ["id" => "7", "name" => "Restaurativa Estetica", "description" => "La Restaurativa Estetica è quella branca dell’Odontoiatria che si occupa della cura dei denti cariati, delle procedure per l’eliminazione della carie.", "img" => "/Media/carie-min.png"],
            ["id" => "8", "name" => "Pedodonzia", "description" => "Non è mai troppo presto per presentare ai nostri bambini il professionista che si prenderà cura del loro insostituibile sorriso, per tutta la vita.", "img" => "/Media/pdo-min.png"],
            ["id" => "9", "name" => "Endodonzia", "description" => "L’Endodonzia è la branca dell’Odontoiatria che si occupa delle terapie dell’endodonto, ovvero dello spazio all’interno degli elementi dentali, dove è contenuta la polpa dentaria.", "img" => "/Media/endo2-min.png"],
            ["id" => "10", "name" => "Protesi Dentale", "description" => "Nei casi clinici in cui l’asportazione del tessuto cariato o la sostituzione di una vecchia otturazione coinvolgano porzioni troppo estese, si ricorre alla Protesi Estetica.", "img" => "/Media/endo-min.png"],
            ["id" => "11", "name" => "Chirurgia Orale", "description" => "Nel Centro Odontoiatrico Idea la paura di sottoporti a un’estrazione, o a un qualsiasi altro trattamento chirurgico, sarà solo un lontanissimo ricordo.", "img" => "/Media/chirurgia-min.png"],
            
        ];

        return view('servizi', ['services' => $services]);
    }


    public function serv($id){

        $id = $id;

        $services = [
            ["id" => "1", "name" => "Igiene dentale", "description" => "La pulizia dei denti, ovvero il trattamento di Igiene Dentale Professionale è spesso il primo al quale si è sottoposti dopo la Visita Specialistica.", "img" => "/Media/igiene-dent.png", "big" => "/Media/pulizia-big.png"],
            ["id" => "2", "name" => "Implantologia", "description" => "L’Implantologia Dentale è quella branca dell’Odontoiatria che si occupa di sostituire i denti mancanti con altrettante radici sintetiche ancorate nell’osso.", "img" => "/Media/impla.png", "big" => "/Media/implant.png"],
            ["id" => "3", "name" => "Paradontologia", "description" => "La Parodontologia è la branca dell’odontoiatria che studia i tessuti del parodonto e le patologie ad esso correlate, definite appunto malattie parodontali.", "img" => "/Media/Paradontologiamin.png", "big" => "/Media/Paradontologia.png"],
            ["id" => "4", "name" => "Faccette dentali", "description" => "Le Faccette Dentali Estetiche vengono realizzate con una ceramica di ultimissima generazione, sue proprietà sono del tutto simili a quelle dello smalto dentale.", "img" => "/Media/faccdent-min.png", "big" => "/Media/faccdent.png"],
            ["id" => "5", "name" => "Sbiancamento Professionale", "description" => "Per effetuare lo sbiancamento dei denti l’operatore applicherà una sostanza isolante sulle vostre gengive che garantirà la protezione dei vostri tessuti molli.", "img" => "/Media/sbdenti-min.png", "big" => "/Media/sbdenti.png"],
            ["id" => "6", "name" => "Ortodonzia Invisibile", "description" => "L’Ortodonzia è quella branca dell’odontoiatria che si occupa delle diverse anomalie della costituzione, dello sviluppo e della posizione dei denti e delle ossa mascellari.", "img" => "/Media/ortodonz-min.png", "big" => "/Media/ortodonz.png"],
            ["id" => "7", "name" => "Restaurativa Estetica", "description" => "La Restaurativa Estetica è quella branca dell’Odontoiatria che si occupa della cura dei denti cariati, delle procedure per l’eliminazione della carie.", "img" => "/Media/carie-min.png", "big" => "/Media/carie.png"],
            ["id" => "8", "name" => "Pedodonzia", "description" => "Non è mai troppo presto per presentare ai nostri bambini il professionista che si prenderà cura del loro insostituibile sorriso, per tutta la vita.", "img" => "/Media/pdo-min.png", "big" => "/Media/pedo.png"],
            ["id" => "9", "name" => "Endodonzia", "description" => "L’Endodonzia è la branca dell’Odontoiatria che si occupa delle terapie dell’endodonto, ovvero dello spazio all’interno degli elementi dentali, dove è contenuta la polpa dentaria.", "img" => "/Media/endo2-min.png", "big" => "/Media/endo2.png"],
            ["id" => "10", "name" => "Protesi Dentale", "description" => "Nei casi clinici in cui l’asportazione del tessuto cariato o la sostituzione di una vecchia otturazione coinvolgano porzioni troppo estese, si ricorre alla Protesi Estetica.", "img" => "/Media/endo-min.png", "big" => "/Media/endo.png"],
            ["id" => "11", "name" => "Chirurgia Orale", "description" => "Nel Centro Odontoiatrico Idea la paura di sottoporti a un’estrazione, o a un qualsiasi altro trattamento chirurgico, sarà solo un lontanissimo ricordo.", "img" => "/Media/chirurgia-min.png", "big" => "/Media/chirurgia.png"],
            
        ];

        return view('servizi-dettaglio', compact('id', 'services'));
    }

    public function submit(Request $req){
        $name = $req -> input('name');
        $email = $req -> input('email');
        $phone = $req -> input('phone');
        $text = $req -> input('text');

        $contact = compact('name', 'email', 'phone', 'text');

        Mail::to($email)->send(new ContactMail($contact));
    

    return redirect(route('homepage'))-> with ('flash', 'Il tuo messaggio è stato inoltrato. Grazie!');

    }


    
}

