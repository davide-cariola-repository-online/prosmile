<x-layout>

 <!-- CONTATTAMI -->
 <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    
</head>
<div class="container mt-5">
    <div id="contattami" class="row">
        <h2 class="tc-main mb-3">Contatti</h2>

        <div class="row justify-content-center">
            <a class="tc-main" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
            <div class="col-12 mt-3 d-flex" id="parent">
                    <div class="col-12 col-lg-6 p-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3005.879756419434!2d16.847549115547586!3d41.11531707929004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1347e8ef8280337f%3A0x924ca264c9c5f4d0!2sStr.%20S.%20Giorgio%20Martire%2C%2070124%20Bari%20BA!5e0!3m2!1sit!2sit!4v1618091839049!5m2!1sit!2sit" style="border:0" frameborder="0" width="100%" height="320px" allowfullscreen></iframe>
                    </div>

                    <div class="col-12 col-lg-6 p-4">
                        <form class="contact-form" method="POST" action="{{route('contatti.submit')}}">

                            @csrf

                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nome e cognome" required="true">
                            </div>
                        
                            <div class="form-group form_left">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="true">
                            </div>
                        
                            <div class="form-group">
                                <input type="number" class="form-control" id="phone" name="phone" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" placeholder="Numero di cellulare" required="true">
                            </div>

                            <div class="form-group">
                                <textarea class="form-control textarea-contact" rows="5" id="comment" name="text" placeholder="Scrivi il tuo messaggio qui..." required="true"></textarea>
                                <br>
                                <button type="submit" class="btn button tc-main"><i class="fas pe-2 fa-paper-plane"></i>INVIA</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container second-portion">
            <div class="row">
                <!-- Boxes de Acoes -->
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="box">							
                        <div class="icon">
                            <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <div class="info">
                                <h6 class="mt-4 tc-sec">EMAIL</h6>
                                <p>
                                    <i class="fa fa-envelope" aria-hidden="true"></i> info@prosmile.it
                                    <br>
                                </p>
                            
                            </div>
                        </div>
                        <div class="space"></div>
                    </div> 
                </div>
                    
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="box">							
                        <div class="icon">
                            <div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                            <div class="info">
                                <h6 class="tc-sec mt-4">CONTATTI</h6>
                                <p>
                                    <i class="fa fa-mobile mb-2" aria-hidden="true"></i> &nbsp (+39)-123456789
                                    <br>
                                    <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp  (+39)-123456789
                                </p>
                            </div>
                        </div>
                        <div class="space"></div>
                    </div> 
                </div>
                    
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="box">							
                        <div class="icon">
                            <div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                            <div class="info">
                                <h6 class="tc-sec mt-4">INDIRIZZO</h6>
                                <p>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> Strada S. Giorgio Martire s.n.c., 70124 Bari (BA)
                                </p>
                            </div>
                        </div>
                        <div class="space"></div>
                    </div> 
                </div>		            
            </div>
        </div>
        <a class="tc-main mb-5" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
    </div>
</div>







</x-layout>