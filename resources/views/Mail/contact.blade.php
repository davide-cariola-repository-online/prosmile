<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Grazie {{$contact['name']}} per averci contattato.</h1>
    <p>Verrai ricontattato entro 48h dal nostro team di esperti.</p>

    <h3>Sei tu?</h3>
    <p>Riepilogo dati:</p>
    <ul>
        <li>
            Nome: {{$contact['name']}};
        </li>
        <li>
            Indirizzo Mail: {{$contact['email']}};
        </li>
        <li>
            Numero di Cellulare: {{$contact['phone']}};
        </li>
        <li>
            Messaggio: {{$contact['text']}}
        </li>
    </ul>
    <br>
    <br>
    <br>
    <div>A presto,</div>
    <i>Il Team proSMILE</i>
</body>
</html>