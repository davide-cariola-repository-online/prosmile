<x-layout>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <h2 class="tc-main">I nostri Servizi</h2>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <a class="tc-main" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
                
                
                @foreach($services as $service)
                <span class="col-12 col-md-5 col-xl-3 col-xxl-2 card m-3 text-center">
                    <x-serviceCard
                        id="{{$service['id']}}"
                        name="{{$service['name']}}"
                        description="{{$service['description']}}"
                        img="{{$service['img']}}"
                        />
                    </span>
                @endforeach
    
                
        </div>
        <a class="tc-main p-4" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
    </div>





</x-layout>