@props(['id', 'name', 'description', 'img'])

      <p><img src="{{$img}}" class="img-fluid photo" alt="foto-servizi"></p>
      <h5 class="tc-main">{{$name}}</h5>
      <small class="mb-5 tc-sec"><i>{!!$description!!}</i></small>
      <a class="review-link link-position tc-accent" href="{{route('servizi-dettaglio', ['id' => $id])}}"><italic>Scopri di più<italic></a>
