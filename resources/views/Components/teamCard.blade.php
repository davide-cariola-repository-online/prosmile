@props(['photo', 'name', 'role', 'cv', 'id'])

      <p><img src="{{$photo}}" class="img-fluid" alt="foto-chi-siamo"></p>
      <h5 class="tc-main">{{$name}}</h5>
      <small class="mb-2 tc-sec"><i>{{$role}}</i></small>
      <p>{!!$cv!!}</p>
