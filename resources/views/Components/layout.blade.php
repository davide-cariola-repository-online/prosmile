<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Link a script --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="shortcut icon" href="/Media/Mini-logo.png" type="image/x-icon">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />


    <title>Pro-Smile ☺</title>
  </head>
  <body>
      {{-- NAVBAR --}}
    <nav class="navbar navbar-expand-lg navbar-light bg-accent sticky-top shadow">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{Route('homepage')}}">proSMILE <i class="fas fa-smile-wink"></i></a>
          
          <button class="navbar-toggler" id="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <i id="toggler" class="fas fa-toggle-off"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link tc-accent-light" href="{{Route('homepage')}}">Homepage</a>
              <a class="nav-link tc-accent-light" href="{{Route('chisiamo')}}">Chi siamo</a>
              <a class="nav-link tc-accent-light" href="{{Route('servizi')}}">Servizi</a>
              <a class="nav-link tc-accent-light" href="{{Route('contatti')}}">Contatti</a>
            </div>
          </div>
        </div>
      </nav>


    {{$slot}}




<!-- GO UP BUTTON -->
<a class="text-center" href="#" id="myBtn" title="Go to top"><i class="pb-3 fas fa-arrow-up"></i></a>


<!-- FOOTER -->

<footer class="footer container-fluid section-margin bg-accent m-0 px-5">
  <div class="row">
      <div class="container-fluid bg-footer pt-5 pb-3">
        <div class="row footer-text">

          <div class="col-12 col-sm-8"> 
              <h3 class="tc-light">proSMILE <i class="fas fa-smile-wink"></i></h3>
              <p class="fs-5 tc-main">P.Iva <span class="text-white">1234567890</span> - <span class="text-white">Cookie policy</span> - <span class="text-white">privacy policy</span></p>
              <p class="tc-main">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Modi sed fugit, ipsam at <br> nisi maiores quaerat reiciendis eos tenetur! Porro quidem quo.</p>
          </div>

          <div class="col-12 col-sm-4 px-3">
            <h3 class="tc-light fs-2">Social</h3>

            <div class="fs-5 text-white">
              <div><i class="fab fa-facebook-square tc-main fs-4 me-2 pb-2"></i>Facebook</div>
              <div><i class="fab fa-instagram tc-main fs-4 me-2 pb-2"></i>Instagram</div>
              <div><i class="fab fa-linkedin tc-main fs-4 me-2 pb-2"></i>LinkedIn</div>
              <div><i class="fab fa-twitter-square tc-main fs-4 me-2 pb-2"></i>Twitter</div>
            </div>


          </div>
        </div>
      </div>
  </div>



    <script src="{{asset('js/app.js')}}"></script>

  </body>
</html>