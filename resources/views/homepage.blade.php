<x-layout>
@if (session('flash'))
    <div class="alert alert-success">
        {{session('flash')}}
    </div>
@endif


<!-- MASTHEAD -->
<header class="masthead">
<div class="container h-100">
    <div class="row h-100 align-items-center">
        <div class="col-12 text-start pt-5">
            <h1 class="display-1 tc-main shadow-white"><i class="mt-5 display-2 tc-light stroke-black fas fa-smile-wink"></i> proSMILE</h1>
            <h2 class="tc-light stroke-black"><i>I professionisti del tuo sorriso<i/></h2>
                <a href="{{Route('contatti')}}"><button class="btn mt-3"> Contattaci per un preventivo gratuito</button></a>
        </div>
    </div>
</div>
</header>



</x-layout>