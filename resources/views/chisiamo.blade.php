<x-layout>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <h2 class="tc-main">Lo Studio</h2>
            <div class="col-12 col-lg-10">
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Adipisci, saepe. Eos doloribus inventore iusto dolores possimus voluptatum, voluptatibus totam vitae officiis optio dolorum sed. Quod doloribus inventore vero accusamus magni!</p>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Alias ad inventore unde beatae mollitia ipsum ea quisquam, facere quia sunt atque enim perspiciatis iure recusandae facilis. Temporibus sed at autem.</p>
            </div>
        </div>
    </div>

<div class="container mt-5">
    <div class="row justify-content-center">
            <h2 class="tc-main">Il nostro Team</h2>
    </div>
    <div class="row justify-content-center">
        <a class="tc-main" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
            
            
            @foreach($team as $member)
            <span class="col-12 col-md-5 col-lg-3 card m-3 text-center box-position">
                <x-teamCard
                    photo="{{$member['photo']}}"
                    name="{{$member['name']}}"
                    role="{{$member['role']}}"
                    cv="{{$member['cv']}}"
                    id="{{$member['id']}}"
                    />
                </span>
            @endforeach

            
    </div>
    <a class="tc-main mb-4" href="{{route('homepage')}}"><small><i>Torna alla Home</i></small></a>
</div>



    
</x-layout>