<x-layout>

    @foreach ($services as $service)
        
    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
                <h2 class="tc-main">la prevenzione comincia da qui.</h2>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Vuoi sfoderare un sorriso sempre smagliante? Una seduta di Igiene Dentale Professionale presso il nostro Centro ti aiuterà a mantenerlo sano e curato più a lungo, evitando l’insorgere di eventuali problemi come carie, tartaro e infiammazioni del cavo orale e delle gengive.</p>
                    <p>Per eseguire tale trattamento utilizziamo strumenti sonici di ultimissima generazione capaci di rimuovere in modo perfetto tutti gli accumuli di tartaro presenti sulla superficie dei denti e al tempo stesso di ridurre al minimo la sensibilità al dolore.</p>
                </div>
                <h2 class="pt-3 tc-main">Ogni quanto è necessario sottoporsi alla pulizia professionale dei denti?</h2>
                <div class="pt-2 tc-accent">
                    <p>Vuoi sfoderare un sorriso sempre smagliante? Una seduta di Igiene Dentale Professionale presso il nostro Centro ti aiuterà a mantenerlo sano e curato più a lungo, evitando l’insorgere di eventuali problemi come carie, tartaro e infiammazioni del cavo orale e delle gengive.</p>
                    <p>Per eseguire tale trattamento utilizziamo strumenti sonici di ultimissima generazione capaci di rimuovere in modo perfetto tutti gli accumuli di tartaro presenti sulla superficie dei denti e al tempo stesso di ridurre al minimo la sensibilità al dolore.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Se vuoi cambiare espressione alla tua vita, l’Implantologia Dentale è la soluzione ideale per ritrovare il sorriso che hai perso, il gusto di masticare in modo corretto e il piacere di mostrare una dentatura perfetta proprio come lo era una volta!</p>
                    <p>Fiore all’occhiello del nostro Centro, l’Implantologia dentale è una tecnica molto evoluta che permette di sostituire i denti mancanti, attraverso l’applicazione di impianti dentali di ultimissima generazione, realizzati con materiali innovativi, certificati e studiati per integrarsi perfettamente all’osso.</p>
                    <p>I risultati sono visibili appena dopo l’intervento: la tua vita migliorerà e tornerai subito a sentirti più sicuro.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Nel Centro Odontoiatrico proSMILE potrai finalmente dire addio a quegli “irritanti” problemi alle gengive che da un po’ di tempo ti hanno fatto perdere la voglia di sorridere.</p>
                    <p>Il nostro consiglio è di prendere subito un appuntamento per un controllo gratuito per non aggravare la situazione, perché la malattia parodontale è una cosa seria che non può essere ignorata.</p>
                    <p>Il Dott. Simone Fiore, specialista nel settore della Parodontologia, valuterà attentamente la tua situazione, analizzando le cause dell’infiammazione e individuando la terapia più corretta con cui intervenire per risolverla.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Per il Centro Odontoiatrico proSMILE ogni paziente merita di sfoggiare un bel sorriso. È per questo che offre un trattamento estetico e restaurativo di ultima generazione, in grado di correggere le imperfezioni, rendendo l’espressione facciale più armoniosa, attraente e naturale.</p>
                    <p>A supporto di tale tecnica viene utilizzata la Progettazione Virtuale che ci consente di mostrare ai nostri pazienti un anteprima digitale del sorriso, definendo la forma, il colore e la posizione di ogni elemento dentale per ottenere il risultato che hanno sempre desiderato.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Vuoi lasciare tutti a bocca aperta con un sorriso più bianco e luminoso? Affidati al nostro trattamento di Sbiancamento Dentale Professionale e i tuoi denti ritorneranno a brillare già dalla prima applicazione!</p>
                    <p>Esistono varie tecniche di sbiancamento e, prima di eseguirlo nel nostro Centro effettuiamo una visita di controllo per verificare l’esistenza di eventuali patologie come la carie o la presenza di vecchie otturazioni. Fase successiva è una seduta di igiene dentale, necessaria per rimuovere placca, tartaro, pigmentazioni e favorire un risultato ottimale del trattamento sbiancante.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Se il “sorriso perfetto”, come tutti sappiamo, non esiste, il Centro Odontoiatrico Idea offre numerosi trattamenti ortodontici che permettono di correggere i suoi difetti e imperfezioni, favorendo uno sviluppo più armonico e sano dell’apparato dento-facciale!</p>
                    <p>Tali anomalie, infatti, se non adeguatamente corrette, possono diventare in età adolescenziale e adulta non solo un problema estetico, ma anche causa di difficoltà nella masticazione, di disfunzioni a carico dell’apparato respiratorio, nonché di danni gengivali e parodontali.</p>
                    <p>È bene quindi intervenire subito, prendendo un appuntamento per una visita di controllo. Il nostro team di esperti odontoiatri individuerà l’apparecchio dentale giusto per tutelare la salute e la bellezza del tuo sorriso.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Sfoggiare un bel sorriso ha uno straordinario potere. Può cambiare il corso della nostra giornata, migliorare il nostro rapporto con gli altri, comunicare empatia e trasferire un’immagine di noi positiva e attraente.</p>
                    <p>Per avere un sorriso sempre curato, splendente e senza carie, nel Centro Odontoiatrico Idea offriamo Trattamenti di Conservativa Estetica di altissimo livello eseguiti grazie all’ausilio di tecnologie e materiali di ultimissima generazione, attraverso cui siamo in grado di effettuare un restauro dentale perfetto, con risultati inimmaginabili fino a qualche tempo fa.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Non è mai troppo presto per pensare alla salute dei propri denti. Questo vale anche per quella dei nostri figli!</p>
                    <p>L’obiettivo del Centro Odontoiatrico proSMILE è far sorridere tutti i bambini. Per farlo abbiamo scelto di dare tutto il meglio di noi, mettendo a disposizione dei nostri piccoli pazienti non solo esperti specializzati in Odontoiatria Pediatrica, ma anche una sala d’attesa ricca di peluche, libri, pennarelli, macchinine e play station, per rendere meno traumatica e più divertente ogni visita, ma soprattutto per insegnare loro, attraverso il gioco, quali sono le buone abitudini e l’importanza di una corretta igiene orale.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Nel Centro Odontoiatrico proSMILE ci piace entrare nel “vivo” di ogni problema, risolvendolo in maniera definitiva attraverso un accurato Trattamento Endodontico dei canali interni delle radici dei denti, dove sono presenti elementi molto sensibili come i nervi, le vene e le arterie (polpa dentaria).</p>
                    <p>Tale trattamento diventa necessario, ad esempio, quando una carie raggiunge la polpa dentaria, oppure quando è in atto un processo infiammatorio a causa di un dente mal devitalizzato. In questi e in molti altri casi, niente paura: a risolverli tutti in modo perfetto ci pensiamo noi!</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Il tuo sorriso non è più come quello di una volta? Affidandoti al Centro Odontoiatrico proSMILE ti faremo ritrovare quello che hai perso, che si tratti di un dente o di parte dell’arcata dentale, progettando Protesi Dentali Estetiche realizzate a regola d’arte e in grado di replicare perfettamente l’estetica dei tuoi denti nella forma, nel colore e nelle dimensioni.</p>
                    <p>Grazie all’utilizzo di tecnologie all’avanguardia e materiali di ultima generazione, i nostri specialisti progetteranno e confezioneranno per te una protesi dentale di altissima qualità, funzionalità e livello estetico.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @if ($id == $service['id'])

    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <img class="img-fluid mb-5" src="{{$service['big']}}" alt="foto-dettaglio">
                <br>
                <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                <h1 class="tc-main">{{$service['name']}}:</h1>
            </div>
            <div class="col-12 col-lg-10">
                <div class="pt-2 tc-accent">
                    <p>Non sorridi più perché provi un forte senso di disagio e imbarazzo a mostrare i tuoi denti? Con il Trattamento di Chirurgia Estetica del Centro Odontoiatrico proSMILE ti tornerà presto la voglia di farlo, perché per noi il sorriso di ogni cliente ha immenso valore e conosciamo le tecniche più innovative in questo campo per valorizzarlo al massimo.</p>
                    <p>Rivolgendoti al nostro Centro troverai uno staff di professionisti specializzati in grado intervenire sull’architettura dento-gengivale, migliorando l’estetica e la funzionalità del sorriso.</p>
                    <a class="tc-main" href="{{route('servizi')}}"><small><i>Torna ai servizi</i></small></a>
                </div>
            </div>
        </div>
    </div>
        
    @endif


    @endforeach
</x-layout>